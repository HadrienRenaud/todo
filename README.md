# A small todo util

## Installation

```shell script
git clone https://gitlab.com/HadrienRenaud/todo.git
cargo install --path todo
```

## Usage

```
USAGE:
    todo [FLAGS] [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -q, --quiet      Don't print anything on success
    -V, --version    Prints version information

OPTIONS:
    -c, --category <CATEGORY>    The category of the wanted todos [default: *]
    -f, --file <FILE>            A custom todo file [default: ~/notes/todo.md]


SUBCOMMANDS:
    add           Add a todo to the file
    complete      Mark a todo as completed
    delete        Remove a todo from the list
    edit          Edit the todos
    help          Prints this message or the help of the given subcommand(s)
    see           See the todos
    uncomplete    Mark a todo as not completed
```
