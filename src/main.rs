#[macro_use]
extern crate lazy_static;

use std::env;
use std::fs::{File, OpenOptions, read_to_string};
use std::io::Cursor;
use std::io::prelude::*;
use std::process::Command;

use bat::PrettyPrinter;
use clap::{App, Arg, SubCommand};
use regex::Regex;
use skim::prelude::*;


fn _get_filtered_file_content<T: Fn(&&str) -> bool>(filename: &str, predicate: T) -> String {
    let file_content = read_to_string(filename).expect("Unable to open file");
    let content: Vec<&str> = file_content.split('\n').filter(predicate).collect();

    return content.join("\n");
}

fn _select_item(items: String) -> Vec<String> {
    let options = SkimOptions {
        multi: true,
        ..SkimOptions::default()
    };
    let item_reader = SkimItemReader::default();
    let skim_items = item_reader.of_bufread(Cursor::new(items));

    return Skim::run_with(&options, Some(skim_items))
        .map(|out| out.selected_items)
        .unwrap_or_else(|| Vec::new())
        .iter()
        .map(|item| String::from(item.output()))
        .collect();
}

fn _selective_replace(filename: &str, from: &str, to: &str) -> String {
    let filtered_file_content = _get_filtered_file_content(filename, |line| line.starts_with(from));
    let selected_items = _select_item(filtered_file_content);

    let mut file_content = _get_filtered_file_content(filename, |_| true);
    for item in selected_items.iter() {
        file_content = file_content.replace(item.as_str(), item.replace(from, to).as_str());
    }

    let mut dst_file = File::create(filename).expect("Unable to open file");
    write!(dst_file, "{}", file_content).expect("Unable to write to file");

    return selected_items.join("\n").replace(from, to);
}

fn _show_result(result: String) {
    PrettyPrinter::new()
        .language("markdown")
        .input_from_bytes(result.as_bytes())
        .print()
        .expect("Cannot display results");
}

fn _does_line_have_category(line: &str, category: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^\s*- \[[x ]] \((?P<cat>.*)\).*$").unwrap();
    }

    if category.eq("*") {
        return true;
    }

    if !RE.is_match(line) {
        return false;
    }

    let caps = RE.captures(line).expect("Cannot apply regex on category");
    let cat = caps
        .name("cat")
        .expect("No match found for category")
        .as_str();
    return cat.eq(category);
}

fn add(todo_file: &str, task: &str, category: &str, quiet: bool) {
    let mut file = OpenOptions::new()
        .write(true)
        .append(true)
        .create(true)
        .open(todo_file)
        .expect("Unable to open file");

    let mut line = String::new();
    line.push_str("- [ ] ");

    if category.ne("*") {
        line.push_str("(");
        line.push_str(category);
        line.push_str(") ");
    }

    line.push_str(task);
    writeln!(file, "{}", line).expect("Unable to write to file");

    if !quiet {
        _show_result(line);
    }
}

fn complete(todo_file: &str, category: &str, quiet: bool) {
    let mut from = String::from("- [ ]");
    let mut to = String::from("- [x]");
    if category.ne("*") {
        from.push_str(" (");
        from.push_str(category);
        from.push_str(")");
        to.push_str(" (");
        to.push_str(category);
        to.push_str(")");
    }
    let result = _selective_replace(todo_file, from.as_str(), to.as_str());

    if !quiet {
        _show_result(result)
    }
}

fn uncomplete(todo_file: &str, category: &str, quiet: bool) {
    let mut from = String::from("- [x]");
    let mut to = String::from("- [ ]");
    if category.ne("*") {
        from.push_str(" (");
        from.push_str(category);
        from.push_str(")");
        to.push_str(" (");
        to.push_str(category);
        to.push_str(")");
    }
    let result = _selective_replace(todo_file, from.as_str(), to.as_str());

    if !quiet {
        _show_result(result);
    }
}

fn delete(todo_file: &str, category: &str, quiet: bool) {
    let file_content =
        _get_filtered_file_content(todo_file, |line| {
            line.starts_with("- [") && _does_line_have_category(line, category)
        });
    let selected_items = _select_item(file_content);

    if selected_items.len() == 0 {
        return;
    }

    let filtered_content: String = _get_filtered_file_content(todo_file, |line| {
        selected_items.iter().all(|item| item.ne(line))
    });

    let mut dst_file = File::create(todo_file).expect("Unable to open file");
    write!(dst_file, "{}", filtered_content).expect("Unable to write to file");

    let result = selected_items.join("\n");

    if !quiet {
        _show_result(result);
    }
}

fn see(todo_file: &str, category: &str) {
    if category == "*" {
        PrettyPrinter::new()
            .language("markdown")
            .input_file(todo_file)
            .print()
            .expect("Cannot display file");
    } else {
        let file_content =
            _get_filtered_file_content(todo_file, |line| _does_line_have_category(line, category));

        PrettyPrinter::new()
            .language("markdown")
            .input_from_bytes(file_content.as_bytes())
            .print()
            .expect("Cannot display file");
    }
}

fn edit(todo_file: &str) {
    let editor = env::var("EDITOR").unwrap_or(String::from("vi"));

    Command::new(editor)
        .arg(todo_file)
        .status()
        .expect("Cannot edit file");
}

fn main() {
    let mut default_todo_file = dirs::home_dir().unwrap_or_default();
    default_todo_file.push("notes");
    default_todo_file.push("todo.md");

    let matches = App::new("todo")
        .version("0.1.0")
        .about("A simple todo app")
        .author("Hadrien Renaud")
        .subcommand(
            SubCommand::with_name("add")
                .about("Add a todo to the file")
                .arg(
                    Arg::with_name("task")
                        .help("What will be added to the file")
                        .value_name("TASK")
                        .required(true)
                        .index(1),
                ),
        )
        .subcommand(SubCommand::with_name("complete").about("Mark a todo as completed"))
        .subcommand(SubCommand::with_name("uncomplete").about("Mark a todo as not completed"))
        .subcommand(SubCommand::with_name("delete").about("Remove a todo from the list"))
        .subcommand(SubCommand::with_name("see").about("See the todos"))
        .subcommand(SubCommand::with_name("edit").about("Edit the todos"))
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("FILE")
                .help("A custom todo file")
                .takes_value(true)
                .required(false)
                .default_value(default_todo_file.to_str().unwrap_or("./todo.md"))
                .global(true),
        )
        .arg(
            Arg::with_name("quiet")
                .short("q")
                .long("quiet")
                .help("Don't print anything on success")
                .global(true),
        )
        .arg(
            Arg::with_name("category")
                .short("c")
                .long("category")
                .value_name("CATEGORY")
                .help("The category of the wanted todos")
                .default_value("*")
                .takes_value(true)
                .required(false)
                .global(true),
        )
        .get_matches();

    let todo_file = matches.value_of("file").unwrap_or_default();
    let category = matches.value_of("category").unwrap_or_default();
    let quiet = matches.is_present("quiet");

    match matches.subcommand() {
        ("add", Some(sub_match)) => add(
            todo_file,
            sub_match.value_of("task").unwrap_or(""),
            category,
            quiet,
        ),
        ("complete", _) => complete(todo_file, category, quiet),
        ("uncomplete", _) => uncomplete(todo_file, category, quiet),
        ("delete", _) => delete(todo_file, category, quiet),
        ("see", _) => see(todo_file, category),
        ("edit", _) => edit(todo_file),
        _ => println!("{}", matches.usage()),
    }
}
